## Title: Deployment functions
## Desc:  Higher level functions to be called when predicting new data.
##        Should be able to handle more than one file at a time

source("metric_funs.R")
source("conv_funs.R")


## convert a file to several re-encoded versions
downEncoder <- function(file_x) {
  if (dir.exists(".swap_files")) {
    print("Swap folder already exists, overwriting...")
    try(
      files_to_rm <- list.files(".swap_files",
        full.names = T
      )
    )
    try(sapply(files_to_rm, file.remove))
  } else {
    dir.create(".swap_files")
  }

  ffmpegConv(
    file_x = file_x,
    conv_set_x = c("320k", "256k", "192k", "128k"),
    out_folder = ".swap_files"
  )
}


## function to prepare a file for xgBoost feature prep
deployFilePrep <- function(file_x) {
  ## read in file for xg and stack pipes

  file_samped <-
    sampleWav2(file_x,
      out_path = ".wav_swap",
      samp_secs = 6
    )

  wav_newname <- str_remove_all(basename(file_samped), "_")

  file.rename(
    from = file_samped,
    to = paste0(".wav_swap/", wav_newname)
  )

  samp_dc_files <- downEncoder(paste0(".wav_swap/", wav_newname))

  ## remove temprorary file in .wav_swap
  #  file.remove(list.files(".wav_swap", full.names = T))

  samp_dc_files
}

## function to create features
deployFeatsGen <- function(downfiles_x) {

  # sort so 320kbps comes first
  files_ls_sort <- sort(unlist(downfiles_x), decreasing = T)
  #  stop("break")
  samps_ls <- lapply(files_ls_sort, function(x) {
    # include whitespace control
    tuneR::readMP3(gsub("\\", "", x, fixed = T))
  })

  names(samps_ls) <- basename(files_ls_sort)

  feats_gen <-
    resGen(
      ls_x = samps_ls,
      ghost = T,
      f_cut = 16
    )

  # tidy up feature generation output (there are two tables)
  feats_tidy_ls <- lapply(feats_gen, function(x) resTidy(x, deploy = T))

  do.call(rbind, feats_tidy_ls)
}


## Predict on new tb
xgPred <- function(tb_x,
                   model_path = "../models/xgB_model.rds") {
  tb_for_mod <- modPrep(tb_x)

  tb_for_mod$id <- "1"

  # load in xgboost model if it hasnt been already
  if (!exists("xg_model")) {
    xg_model <- readRDS(model_path)
  }

  # generate result
  predict(xg_model, new_data = tb_for_mod) %>%
    mutate(
      .pred = case_when(
        .pred > 288 ~ 320,
        .pred > 224 & .pred <= 288 ~ 256,
        .pred > 160 & .pred <= 224 ~ 192,
        TRUE ~ 128
      )
    ) %>%
    as_vector()
}



## functions to predict for stacked model
stackPred <- function(tb_x,
                      model_path = "../models/stacks_fit.rds") {
  tb_stack <- stackModPrep(tb_x)

  if (!exists("stack_model")) {
    stack_model <- readRDS(model_path)
  }

  predict(stack_model, new_data = tb_stack) %>%
    mutate(
      .pred = case_when(
        .pred > 288 ~ 320,
        .pred > 224 & .pred <= 288 ~ 256,
        .pred > 160 & .pred <= 224 ~ 192,
        TRUE ~ 128
      )
    ) %>%
    as_vector()
}

## a function to wrap around previous xgboost functions
xgWrap <- function(file_x,
                   model_path_w = "../models/xgB_model.rds") {
  require(stringr, quietly = T)
  require(magrittr, quietly = T)
  require(workflows, quietly = T)

  foo_downsamples <- deployFilePrep(file_x)

  foo_feats <- deployFeatsGen(foo_downsamples)

  xgPred(foo_feats,
    model_path = model_path_w
  )
}

stackWrap <- function(file_x,
                      model_path_w = "../models/stacks_fit.rds") {
  require(stringr, quietly = T)
  require(magrittr, quietly = T)
  require(workflows, quietly = T)

  foo_downsamples <- deployFilePrep(file_x)

  foo_feats <- deployFeatsGen(foo_downsamples)

  stackPred(foo_feats, model_path_w)
}


## Function to preprocess a wav or mp3 for slice svm model
slicePrep <- function(file_x) {
  require(tibble, quietly = T)
  require(purrr, quietly = T)

  foo_file <- sampleWav2(
    file_x = file_x,
    samp_secs = 6,
    write_file = F
  )

  samp_freqs <- meanfrqAmps(foo_file, cutoff_fq = 16)

  as_tibble(t(x = samp_freqs)) # %>%
}

## this doesn't like wavs for some reason,
## seems to be underestimating them
slicePred <- function(file_x,
                      model_path = "../models/medmus_svm_simple_model.rds") {
  if (!exists("slice_model")) {
    slice_model <- readRDS(model_path)
  }

  test_slice_proc <- slicePrep(file_x)

  as_vector(predict(slice_model, new_data = test_slice_proc))
}
