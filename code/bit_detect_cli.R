#!/usr/bin/env Rscript

## Title:   Deplotment script
## Author:  x19175329@ncirl.ie
## Desc:    An executable script for MP3 bitrate detection

## check for argparse package
## If it isn't there, prompt for install
if (!"argparser" %in% installed.packages()) {
  cat("The argparser package is required.\nInstall now? (y/n):")
  resp <- readLines("stdin", n = 1)

  if (!resp %in% c("y", "n")) {
    stop("Please enter y or n")
  }

  if (resp == "y") {
    install.packages("argparser", repos = "http://cran.us.r-project.org")
  } else {
    stop("Operations cancelled")
  }
}

## load argparser and step up its arguments
## argparser makes it work like a normal terminal progam
library(argparser)
library(methods)
parser <- arg_parser("MP3 bitrate detector")

parser <- add_argument(parser,
  c("--file", "--model"),
  help = c(
    "Path to MP3 file [required!]",
    "Model to use (psvm, xgb, stack)"
  ),
  short = c("-f", "-m"),
  default = list(NA, "xgb")
)

args_vec <- parse_args(parser)

## validate inputs
if (is.na(args_vec$file)) {
  print(parser)
  stop("Input MP3 path required")
}

if (tools::file_ext(args_vec$file) != "mp3") {
  stop("Please specify MP3 files only")
}

if (!file.exists(args_vec$file)) {
  stop("Could not find file specified")
}


## list package requirements, dependent on model selection
if (any(args_vec$model %in% c("xgb", "stack"))) {
  package_list <- c(
    "tidyverse",
    "catsim",
    "seewave",
    "snakecase",
    "tuneR",
    "kernlab",
    "xgboost",
    "stacks"
  )
} else if (args_vec$model == "psvm") {
  package_list <- c(
    "tidyverse",
    "tidymodels",
    "tuneR",
    "kernlab"
  )
} else {
  stop("Model selection not recognised")
}

## check for package requirements
## if they are not there, prompt user for an install
missing_pkg <- which(!package_list %in% installed.packages()[, 1])

if (length(missing_pkg) > 0) {
  cat("Some additional R packages are required.\nInstall now? (y/n):")
  resp <- readLines("stdin", n = 1)

  if (!resp %in% c("y", "n")) {
    stop("Please enter y or n")
  }

  if (resp == "y") {
    install.packages(package_list[missing_pkg], repos = "http://cran.us.r-project.org")
  } else {
    stop("Operations cancelled")
  }
}

## load in custom functions
source("deploy_funs.R")

## file name might contain spaces, so catch these
file_clean <- gsub("\\", "", args_vec$file, fixed = T)

## run a model with file input
if (args_vec$model == "psvm") {
  raw_output <- slicePred(args_vec$file,
    model_path = "../models/medmus_svm_simple_model.rds"
  )
} else if (args_vec$model == "xgb") {
  raw_output <- xgWrap(args_vec$file,
    model_path = "../models/medmus_xgB_model.rds"
  )
  unlink(".swap_files", recursive = T)
} else if (args_vec$model == "stack") {
  raw_output <- stackWrap(args_vec$file,
    model_path = "../models/medmus_stacks_fit.rds"
  )
  unlink(".swap_files", recursive = T)
}


## cat the cleaned result
cat(as.numeric(as.character(raw_output)), "kBps", "\n")
