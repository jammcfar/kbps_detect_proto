# MP3 bitrate detector (prototype)

Prototype R-based command-line interface (CLI) program to detect MP3 bitrate. It has three different models; a stacked model, an XGBoost based model and an implementation of a polynomial SVM found in the literature (see 10.1145/1597817.1597828).

## Requirements
R (>V.4.0.0)
ffmpeg
BASH (or other POSIX compliant shell)

## Installation and running

### Linux
These steps will likely also work for MAC and other UNIX-likes.
1) Install R using your operating systems package manager.
2) Install ffmpeg using your operating systems package manager.
3) From the terminal:
```
git clone https://gitlab.com/jammcfar/kbps_detect_proto
cd kbps_detect_proto/code
chmod +X bit_detect_cli.R
```
4) Run the program with `./bit_detect_cli.R`. Options will be displayed. To run the program on an actual file, run `./bit_detect_cli.R -f testmp3.mp3`, replacing testmp3.mp3 with the name of your mp3 file.
It will ask to install packages. Some of these may also require system wide libraries. If any of the packages fail to install, attempt a manual installation of these packages from within R. The required package list is
"tidyverse",
"catsim",
"seewave",
"snakecase",
"tuneR",
"kernlab",
"xgboost",
"stacks"

### Windows
Using the program can be done through WSL2. Git Bash may also be possible but is untested and setup may be more complex.
1) Download+Install Windows Terminal.
1) Download+Install WSL2 from windows store.
2) Enable WSL2 unlock in system settings.
3) Open WSL2 terminal
4) Begin the Linux steps above. After installing R, you may need to open it and install a random package so it creates a User specific package library.
5) Some of the packages might complain about systemd not being the init system. They can be ignored.
6) If manually trying to install R packages on WSL2, sometimes permission failures can occur. Use `install.packages("tidyverse", dependencies=TRUE, INSTALL_opts = c('--no-lock'))`

## Known bugs
The program may have trouble with special charaacters in the file names. Remove them if this is the case.
